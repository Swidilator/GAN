# Conditional GAN for image synthesis

Implementation of the network presented in _High-Resolution Image Synthesis and Semantic Manipulation with Conditional GANs_.

## Submodule

This project is a submodule for the project located at _https://gitlab.com/Swidilator/MastersProject_.