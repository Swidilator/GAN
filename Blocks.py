import torch
import torch.nn as nn
import torch.nn.modules as modules

from support_scripts.components.blocks import Block
from support_scripts.utils import norm_selector


class ResidualBlock(Block):
    def __init__(self, filter_count: int, input_channel_count: int):
        super(ResidualBlock, self).__init__(filter_count, input_channel_count)

        kernel_size: int = 3
        stride: int = 1
        padding: int = 1

        self.reflect_pad: nn.ReflectionPad2d = nn.ReflectionPad2d(padding)

        self.conv_1: modules.Conv2d = nn.Conv2d(
            self.input_channel_count,
            self.filter_count,
            kernel_size=kernel_size,
            stride=stride,
            padding=0,
        )
        Block.init_conv_weights(conv=self.conv_1, init_type="normal", zero_bias=False)

        self.instance_norm_1, _ = norm_selector(
            "instance", output_channel_count=self.filter_count
        )

        self.ReLU: nn.ReLU = nn.ReLU(True)

        self.conv_2: modules.Conv2d = nn.Conv2d(
            self.filter_count,
            self.filter_count,
            kernel_size=kernel_size,
            stride=stride,
            padding=0,
        )
        Block.init_conv_weights(conv=self.conv_2, init_type="normal", zero_bias=False)

        self.instance_norm_2, _ = norm_selector(
            "instance", output_channel_count=self.filter_count
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        out: torch.Tensor

        out = self.reflect_pad(x)
        out = self.conv_1(out)
        out = self.instance_norm_1(out)
        out = self.ReLU(out)
        out = self.reflect_pad(out)
        out = self.conv_2(out)
        out = self.instance_norm_2(out)

        return x + out


class CCIRBlock(Block):
    def __init__(
        self,
        filter_count: int,
        input_channel_count: int,
        relu_flag: bool = True,
        norm_flag: bool = True,
    ):
        super(CCIRBlock, self).__init__(filter_count, input_channel_count)

        self.relu_flag: bool = relu_flag
        self.norm_flag: bool = norm_flag

        kernel_size: int = 7
        stride: int = 1
        padding: int = 3

        self.reflect_pad: nn.ReflectionPad2d = nn.ReflectionPad2d(padding)

        self.conv_1: modules.Conv2d = nn.Conv2d(
            self.input_channel_count,
            self.filter_count,
            kernel_size=kernel_size,
            stride=stride,
        )
        Block.init_conv_weights(conv=self.conv_1, init_type="normal", zero_bias=False)

        if self.norm_flag:
            self.instance_norm_1, _ = norm_selector(
                "instance", output_channel_count=self.filter_count
            )

        if self.relu_flag:
            self.ReLU: nn.ReLU = nn.ReLU(True)

    def forward(self, input_tensor: torch.Tensor) -> torch.Tensor:
        out: torch.Tensor = self.reflect_pad(input_tensor)
        out = self.conv_1(out)
        if self.norm_flag:
            out = self.instance_norm_1(out)
        if self.relu_flag:
            out = self.ReLU(out)
        return out


class DCIRBlock(Block):
    def __init__(self, filter_count: int, input_channel_count: int, stride: int = 2):
        super(DCIRBlock, self).__init__(filter_count, input_channel_count)

        kernel_size: int = 3
        stride: int = stride
        padding: int = 1

        self.conv_1: modules.Conv2d = nn.Conv2d(
            self.input_channel_count,
            self.filter_count,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
        )
        Block.init_conv_weights(conv=self.conv_1, init_type="normal", zero_bias=False)

        self.instance_norm_1, _ = norm_selector(
            "instance", output_channel_count=self.filter_count
        )

        self.ReLU: nn.ReLU = nn.ReLU(True)

    def forward(self, input_tensor: torch.Tensor) -> torch.Tensor:
        out = self.conv_1(input_tensor)
        out = self.instance_norm_1(out)
        out = self.ReLU(out)
        return out


class UCIRBlock(Block):
    def __init__(self, filter_count: int, input_channel_count: int, stride=2):
        super(UCIRBlock, self).__init__(filter_count, input_channel_count)

        kernel_size: int = 3
        stride: float = stride
        padding: int = 1
        output_padding: int = int(stride // 2)

        self.deconv_1: nn.ConvTranspose2d = nn.ConvTranspose2d(
            self.input_channel_count,
            self.filter_count,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            output_padding=output_padding,
        )
        Block.init_conv_weights(conv=self.deconv_1, init_type="normal", zero_bias=False)

        self.instance_norm_1, _ = norm_selector(
            "instance", output_channel_count=self.filter_count
        )

        self.ReLU: nn.ReLU = nn.ReLU(True)

    def forward(self, input_tensor: torch.Tensor) -> torch.Tensor:
        out: torch.Tensor = self.deconv_1(input_tensor)
        out = self.instance_norm_1(out)
        out = self.ReLU(out)
        return out
