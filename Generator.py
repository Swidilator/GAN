from typing import Tuple, Optional

from GAN.Blocks import *
from support_scripts.components import FlowNetWrapper


class Generator(torch.nn.Module):
    def __init__(
        self,
        use_tanh: bool,
        num_classes: int,
        use_feature_encoder: bool,
        num_prior_frames: int,
        use_optical_flow: bool,
        use_edge_map: bool,
        use_twin_network: bool,
        use_local_enhancer: bool,
        final_image_size: Tuple[int, int],
        normalised_prior_frames: bool,
        use_simple_warped_image_merging: bool,
    ):
        super(Generator, self).__init__()

        self.use_tanh: bool = use_tanh
        self.num_classes: int = num_classes
        self.use_feature_encoder: bool = use_feature_encoder
        self.num_prior_frames: int = num_prior_frames
        self.use_optical_flow: bool = use_optical_flow
        self.use_edge_map: bool = use_edge_map
        self.use_twin_network: bool = use_twin_network
        self.use_local_enhancer: bool = use_local_enhancer
        self.final_image_size: Tuple[int, int] = final_image_size
        self.normalised_prior_frames: bool = normalised_prior_frames
        self.use_simple_warped_image_merging: bool = use_simple_warped_image_merging

        # Check for correct settings
        if self.use_optical_flow:
            assert (
                self.num_prior_frames > 0
            ), "num_prior_frames > 0 required if use_optical_flow == True"

        if self.use_twin_network:
            assert (
                self.num_prior_frames > 0
            ), "self.use_twin_network is True, but self.num_prior_frames == 0."

        assert not (
            self.use_local_enhancer and (self.use_twin_network or self.use_optical_flow)
        ), "Local enhancer cannot be used with twin network or optical flow."

        if self.use_optical_flow:
            assert (
                self.use_twin_network
            ), "self.use_optical_flow is True, but self.use_twin_network is False."

        # Calculate input channel counts
        self.total_image_input_channel_count: int = num_prior_frames * 3

        # Total number of input channels
        self.total_semantic_input_channel_count: int = (
            (num_classes * (num_prior_frames + 1))
            + (use_feature_encoder * 3)
            + (use_edge_map * 1)
            + (self.total_image_input_channel_count if not self.use_twin_network else 0)
        )

        # Global generator / twin optical flow network
        self.global_generator: GlobalGenerator = GlobalGenerator(
            self.total_semantic_input_channel_count,
            self.total_image_input_channel_count,
            use_twin_network,
            use_optical_flow,
            use_tanh,
            self.final_image_size,
            not self.use_simple_warped_image_merging,
        )

        # Local enhancer
        if use_local_enhancer:
            self.local_enhancer: LocalEnhancer = LocalEnhancer(
                self.total_semantic_input_channel_count, use_tanh
            )

        # Downsampling
        self.downsample = nn.AvgPool2d(
            3, stride=2, padding=[1, 1], count_include_pad=False
        )

        # Grid for warping
        if self.use_optical_flow:
            self.grid: torch.Tensor = FlowNetWrapper.get_grid(
                1, self.final_image_size, torch.device("cuda:0")
            )

    def forward(
        self,
        msk: torch.Tensor,
        feature_encoding: Optional[torch.Tensor],
        edge_map: Optional[torch.Tensor],
        prev_images: Optional[torch.Tensor] = None,
        prev_masks: Optional[torch.Tensor] = None,
    ) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:

        # Semantic input
        input_semantic_list: list = [msk]
        if feature_encoding is not None:
            input_semantic_list.append(feature_encoding)
        if edge_map is not None:
            input_semantic_list.append(edge_map)

        if self.num_prior_frames > 0:
            input_semantic_list.append(prev_masks)
            if not self.use_twin_network:
                input_semantic_list.append(prev_images)

        input_semantic: torch.Tensor = torch.cat(input_semantic_list, dim=1)

        output: torch.Tensor
        output_gen: Optional[torch.Tensor] = None
        output_warped: Optional[torch.Tensor] = None
        output_flow: Optional[torch.Tensor] = None
        output_mask: Optional[torch.Tensor] = None

        # Global generator only
        if not self.use_local_enhancer:
            _, output_gen, output_flow, output_mask = self.global_generator(
                input_semantic, prev_images
            )

            # Optical flow and merge mask
            if self.use_optical_flow:
                # Warp prior frame with flow
                output_warped: Optional[torch.Tensor] = FlowNetWrapper.resample(
                    prev_images[:, 0:3] + (self.normalised_prior_frames * 0.5),
                    output_flow,
                    self.grid,
                )
                # Merge warped frame and generated frame
                if not self.use_simple_warped_image_merging:
                    output: torch.Tensor = (output_mask * output_gen) + (
                        (torch.ones_like(output_mask) - output_mask) * output_warped
                    )
                else:
                    output: torch.Tensor = (output_gen + output_warped) / 2
            else:
                output = output_gen
                output_gen = None

        else:
            smaller_input = self.downsample(input_semantic)

            out, _, _, _ = self.global_generator(smaller_input, None)

            output: torch.Tensor = self.local_enhancer((input_semantic, out))

        # Add num_images dim
        output = output.unsqueeze(1)

        return (
            output,
            output_gen,
            output_warped,
            output_flow,
            output_mask,
        )


class GeneratorDownsample(torch.nn.Module):
    def __init__(self, input_channel_count):
        super(GeneratorDownsample, self).__init__()
        self.c1: CCIRBlock = CCIRBlock(64, input_channel_count)

        self.d2: DCIRBlock = DCIRBlock(128, self.c1.get_output_filter_count)
        self.d3: DCIRBlock = DCIRBlock(256, self.d2.get_output_filter_count)
        self.d4: DCIRBlock = DCIRBlock(512, self.d3.get_output_filter_count)
        self.d5: DCIRBlock = DCIRBlock(1024, self.d4.get_output_filter_count)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        out: torch.Tensor = self.c1(input_data)

        out = self.d2(out)
        out = self.d3(out)
        out = self.d4(out)
        out = self.d5(out)
        return out


class GeneratorResnetProcessor(torch.nn.Module):
    def __init__(self, num_resnet_blocks: int):
        super(GeneratorResnetProcessor, self).__init__()
        resnet_list: list = []

        for i in range(num_resnet_blocks):
            resnet_list.append(ResidualBlock(1024, 1024))

        self.resnet_seq: torch.nn.Sequential = torch.nn.Sequential(*resnet_list)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        return self.resnet_seq(input_data)


class GeneratorUpsample(torch.nn.Module):
    def __init__(self):
        super(GeneratorUpsample, self).__init__()
        self.u1: UCIRBlock = UCIRBlock(512, 1024)
        self.u2: UCIRBlock = UCIRBlock(256, self.u1.get_output_filter_count)
        self.u3: UCIRBlock = UCIRBlock(128, self.u2.get_output_filter_count)
        self.u4: UCIRBlock = UCIRBlock(64, self.u3.get_output_filter_count)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        out = self.u1(input_data)
        out = self.u2(out)
        out = self.u3(out)
        out = self.u4(out)
        return out


class GlobalGenerator(torch.nn.Module):
    def __init__(
        self,
        input_semantic_channel_count: int,
        input_image_channel_count: int,
        use_twin_network: bool,
        use_optical_flow: bool,
        use_tanh: bool,
        final_image_size: Tuple[int, int],
        output_flow_mask: bool,
    ) -> None:
        super(GlobalGenerator, self).__init__()

        self.use_twin_network: bool = use_twin_network
        self.use_optical_flow: bool = use_optical_flow
        self.use_tanh: bool = use_tanh
        self.final_image_size: Tuple[int, int] = final_image_size
        self.output_flow_mask: bool = output_flow_mask

        # Build network from flexible pieces
        self.downsample: GeneratorDownsample = GeneratorDownsample(
            input_semantic_channel_count
        )

        self.resnet_processor_1: GeneratorResnetProcessor = GeneratorResnetProcessor(5)
        self.resnet_processor_2: GeneratorResnetProcessor = GeneratorResnetProcessor(4)

        self.upsample: GeneratorUpsample = GeneratorUpsample()

        self.final_image_block: CCIRBlock = CCIRBlock(
            3, 64, relu_flag=False, norm_flag=False
        )

        if self.use_tanh:
            self.tan_h = nn.Tanh()

        if self.use_twin_network:
            self.downsample_twin: GeneratorDownsample = GeneratorDownsample(
                input_image_channel_count
            )

            self.resnet_processor_1_twin: GeneratorResnetProcessor = (
                GeneratorResnetProcessor(5)
            )
            # If using a twin network, extra upsampling for optical flow
            if self.use_optical_flow:
                self.resnet_processor_2_twin: GeneratorResnetProcessor = (
                    GeneratorResnetProcessor(4)
                )

                self.upsample_twin: GeneratorUpsample = GeneratorUpsample()

        if self.use_optical_flow:
            # Conv for generating optical flow
            self.flow_conv_out: nn.Sequential = nn.Sequential(
                nn.ReflectionPad2d(3),
                nn.Conv2d(
                    64,
                    2,
                    kernel_size=7,
                    padding=0,
                ),
            )

            if self.output_flow_mask:
                # Conv for generating merge mask
                self.mask_conv_out: nn.Sequential = nn.Sequential(
                    nn.ReflectionPad2d(3),
                    nn.Conv2d(
                        64,
                        1,
                        kernel_size=7,
                        padding=0,
                    ),
                    nn.Sigmoid(),
                )

    def forward(
        self, input_semantic_data: torch.Tensor, input_image_data: torch.Tensor
    ) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:

        out: torch.Tensor = self.downsample(input_semantic_data)
        out_centre = self.resnet_processor_1(out)
        if self.use_twin_network:
            out_centre += self.resnet_processor_1_twin(
                self.downsample_twin(input_image_data)
            )
        out = self.resnet_processor_2(out_centre)
        out = self.upsample(out)

        out_flow = None
        out_mask = None

        # Use second half of twin network only if using flow
        if self.use_twin_network and self.use_optical_flow:
            out_twin: torch.Tensor = self.upsample_twin(
                self.resnet_processor_2_twin(out_centre)
            )
            out_flow: Optional[torch.Tensor] = self.flow_conv_out(out_twin)
            if self.output_flow_mask:
                out_mask: Optional[torch.Tensor] = self.mask_conv_out(out_twin)

        out_image = self.final_image_block(out)

        if self.use_tanh:
            out_image = self.tan_h(out_image).clone()

        out_image = (out_image + 1.0) / 2.0

        return out, out_image, out_flow, out_mask


class LocalEnhancer(torch.nn.Module):
    def __init__(self, input_channel_count: int, use_tanh: bool):
        super(LocalEnhancer, self).__init__()

        self.use_tanh: bool = use_tanh

        self.reflect_pad: nn.ReflectionPad2d = nn.ReflectionPad2d(3)
        self.c1: CCIRBlock = CCIRBlock(32, input_channel_count)

        self.d2: DCIRBlock = DCIRBlock(64, self.c1.get_output_filter_count)

        self.r3: ResidualBlock = ResidualBlock(64, self.d2.get_output_filter_count)
        self.r4: ResidualBlock = ResidualBlock(64, self.r3.get_output_filter_count)
        self.r5: ResidualBlock = ResidualBlock(64, self.r4.get_output_filter_count)

        self.u6: UCIRBlock = UCIRBlock(32, self.r5.get_output_filter_count)

        self.c7: CCIRBlock = CCIRBlock(
            3, self.u6.get_output_filter_count, relu_flag=False, norm_flag=False
        )

        if self.use_tanh:
            self.tan_h = nn.Tanh()

    def forward(self, inputs: Tuple[torch.Tensor, torch.Tensor]) -> torch.Tensor:
        out: torch.Tensor = self.c1(inputs[0])

        # Add the output of the global generator
        out = self.d2(out) + inputs[1]

        out = self.r3(out)
        out = self.r4(out)
        out = self.r5(out)

        out = self.u6(out)
        out_image = self.c7(out)

        if self.use_tanh:
            out_image = self.tan_h(out_image).clone()

        out_image = (out_image + 1.0) / 2.0

        return out_image
